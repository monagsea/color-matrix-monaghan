package com.example.colormatrixapplication.view

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.colormatrixapplication.databinding.ItemColorBinding

class RecyclerAdapter : RecyclerView.Adapter<RecyclerAdapter.ColorsViewHolder>() {

    private var colors = mutableListOf<Int>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ColorsViewHolder {
        val binding = ItemColorBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return ColorsViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ColorsViewHolder, position: Int) {
        val color = colors[position]
        holder.loadColor(color)
    }

    override fun getItemCount(): Int {
        return colors.size
    }

    fun addColors(colors: List<Int>) {
        this.colors = colors.toMutableList()
        notifyDataSetChanged()
    }

    class ColorsViewHolder(
        private val binding: ItemColorBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun loadColor(color: Int) {
            binding.ivColor.setBackgroundColor(color)
            binding.ivColor.setOnClickListener {
                it.findNavController().navigate(ColorFragmentDirections.actionColorFragmentToNumbersFragment(color.toString().drop(1)))
            }
        }
    }
}