package com.example.colormatrixapplication.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.colormatrixapplication.databinding.ItemNumberBinding

class RecyclerAdapterNumbers : RecyclerView.Adapter<RecyclerAdapterNumbers.NumbersViewHolder>() {

    private var numbers = listOf<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NumbersViewHolder {
        val binding = ItemNumberBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return NumbersViewHolder(binding)
    }

    override fun onBindViewHolder(holder: NumbersViewHolder, position: Int) {
        val number = numbers[position]
        holder.loadNumber(number)
    }

    override fun getItemCount(): Int {
        return numbers.size
    }

    fun addNumbers(numbers: List<String>) {
        this.numbers = numbers.toMutableList()
        notifyDataSetChanged()
    }

    class NumbersViewHolder(
        private val binding: ItemNumberBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun loadNumber(number: String) {
            binding.tvLetter.text = number
        }
    }
}