package com.example.colormatrixapplication.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.children
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.example.colormatrixapplication.databinding.FragmentColorBinding
import com.example.colormatrixapplication.helper.randomColor
import com.example.colormatrixapplication.viewmodel.ColorViewModel


class ColorFragment : Fragment() {

    var newColors: MutableList<Int> = mutableListOf<Int>()

    private var _binding: FragmentColorBinding? = null
    private val binding get() = _binding!!
    private val colorViewModel by viewModels<ColorViewModel>()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentColorBinding.inflate(
        inflater, container, false
    ).also { _binding = it }.root

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.button.setOnClickListener {
            binding.rvList.layoutManager = GridLayoutManager(this.context, 3)
            binding.rvList.adapter = RecyclerAdapter().apply {
                val repeater = binding.etColors.text.toString().toInt()
                colorViewModel.getColors(repeater)
                colorViewModel.colors.observe(viewLifecycleOwner) {
                    addColors(it)
                }
            }
        }
        binding.rvList.rootView.setOnClickListener {
            Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show()
        }
    }
}

