package com.example.colormatrixapplication.view

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.colormatrixapplication.R
import com.example.colormatrixapplication.databinding.FragmentColorBinding
import com.example.colormatrixapplication.databinding.FragmentNumbersBinding
import com.example.colormatrixapplication.viewmodel.ColorViewModel


class NumbersFragment : Fragment() {

    private var _binding: FragmentNumbersBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentNumbersBinding.inflate(
        inflater, container, false
    ).also { _binding = it }.root

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val passedColor = arguments?.getString("number")
        var splitNumber = passedColor?.split("")?.toList()

        binding.rvLetterList.layoutManager = LinearLayoutManager(this.context)
        binding.rvLetterList.adapter = RecyclerAdapterNumbers().apply {
            splitNumber?.let { addNumbers(it) }
        }
    }
}