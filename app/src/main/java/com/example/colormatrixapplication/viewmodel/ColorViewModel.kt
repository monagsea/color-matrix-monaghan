package com.example.colormatrixapplication.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.colormatrixapplication.model.ColorRepo
import kotlinx.coroutines.launch

class ColorViewModel : ViewModel() {
    private val repo = ColorRepo

    private val _color = MutableLiveData<List<Int>>()
    val colors : LiveData<List<Int>> get() = _color

    fun getColors(repeater: Int) {
        viewModelScope.launch {
            val colorsList = repo.getColors(repeater)
            _color.value = colorsList
        }
    }
}