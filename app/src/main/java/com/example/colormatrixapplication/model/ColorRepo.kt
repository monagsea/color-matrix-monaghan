package com.example.colormatrixapplication.model

import com.example.colormatrixapplication.helper.randomColor
import com.example.colormatrixapplication.model.remote.ColorApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object ColorRepo {
    private val ColorApi = object : ColorApi {
        override suspend fun getColors(repeater: Int) : List<Int> {
            var newColors: MutableList<Int> = mutableListOf()
            repeat(repeater) {
                newColors.add(randomColor)
            }
            return newColors
        }
    }

    suspend fun getColors(repeater: Int): List<Int> = withContext(Dispatchers.IO)
    {
        ColorApi.getColors(repeater)
    }
}