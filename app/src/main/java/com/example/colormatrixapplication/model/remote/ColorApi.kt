package com.example.colormatrixapplication.model.remote

interface ColorApi {
    suspend fun getColors(repeater: Int) : List<Int>
}